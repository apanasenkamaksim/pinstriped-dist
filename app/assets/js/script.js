jQuery(document).ready(function($) {

  var bs_collapse_nav=$("#bs-example-navbar-collapse-1"),
    scrollTop=$(document).scrollTop(), 
    lastScrollTop=$(document).scrollTop();

  sections_min_height();
  navbar_bg_color();
  sticker_position();


  // $(document).on("scroll",scrollBySections);
  $(document).on("scroll", onScroll);
  $(window).on("resize", onResize);


  function onScroll(){
    scrollTop=$(document).scrollTop();
    navbar_bg_color();
    sticker_position();
  }
  function onResize(){
    sections_min_height();
    navbar_bg_color();
    sticker_position();
  }


  bs_collapse_nav.on('show.bs.collapse', function () {
    $(".page-header nav").css({"background-color": "#000"});
  });
  bs_collapse_nav.on('hidden.bs.collapse', function () {
    navbar_bg_color();
  });

  function sections_min_height(){
    $(".page-body .full-height:visible").css({"min-height": $(window).height()});
    if($(window).width()>=992){
      $(".page-body .full-height-md:visible").css({"min-height": $(window).height()});
    }else{
      $(".page-body .full-height-md:visible").css({"min-height": 0});
    }
  }

  function scrollBySections(){
    if($(".onscroll:visible").length==0)return;
    
    scrollTop=$(document).scrollTop();

    $(".onscroll:visible").each(function() {
     if(lastScrollTop>=$(this).offset().top&&lastScrollTop<$(this).offset().top+$(this).height()){
      currentIndex=$(".onscroll:visible").index($(this));
      return false;
     }
    });

    if (scrollTop > lastScrollTop){
       // downscroll code
       toSection=$(".onscroll:visible").eq(currentIndex+1);
    } else if (scrollTop < lastScrollTop){
      // upscroll code
      toSection=$(".onscroll:visible").eq(currentIndex-1);
    }else{
      return;
    }

    if(toSection.length>0){
      $(document).unbind("scroll",scrollBySections);

      $('html, body').stop().animate({
        'scrollTop': toSection.offset().top
      }, 1000, 'swing', function() {
        $(document).on("scroll", scrollBySections);
        lastScrollTop=$(document).scrollTop();
      });
    }else{
      lastScrollTop=$(document).scrollTop();
    }
  };

  function navbar_bg_color(){
     $(".page-header nav").css({"background-color": (scrollTop>0||(bs_collapse_nav.is(":visible")&&$(window).width()<768))?"#000":"transparent"});
  }

  function sticker_position(){
    topOffset=80;
    bottomOffset=80;

    sticker=$("#sticker");
    if(sticker.length==0)return;

    parent=sticker.parent();
    topPosition=scrollTop-parent.offset().top+$(window).innerHeight()/2-sticker.height()/2+topOffset;
    bottomPosition=topPosition+sticker.height()+bottomOffset;

    if(scrollTop<parent.offset().top+topOffset-$(window).innerHeight()/2+sticker.height()/2-$(".page-header nav").outerHeight()/2){
      sticker.css({position: "absolute"});
      sticker.css({top: topOffset});
      sticker.css({bottom: "auto"});
      sticker.css({left: 10});
    }else if(scrollTop>parent.offset().top+parent.height()-bottomOffset-$(window).innerHeight()/2-sticker.height()/2-$(".page-header nav").outerHeight()/2){
      sticker.css({position: "absolute"});
      sticker.css({top: "auto"});
      sticker.css({bottom: bottomOffset});
      sticker.css({left: 10});
    }else{
      // sticker.css({top: parent.height()-sticker.height()-bottomOffset});
      sticker.css({position: "fixed"});
      sticker.css({top: $(window).innerHeight()/2-sticker.height()/2+$(".page-header nav").outerHeight()/2});
      sticker.css({bottom: "auto"});
      sticker.css({left: parent.offset().left+10});
    }


    stickerValue=$("#sticker .value");

    from=parent.offset().top;
    to=parent.offset().top+parent.height()-$(window).innerHeight()/2;
    now=scrollTop+$(window).innerHeight()/2;
    
    value=(now-from)/(to-from)*100;

    if(value<0){
      stickerValue.css({height: 0+"%"});
    }else if(value<=100){
      stickerValue.css({height: value+"%"});
    }else{
      stickerValue.css({height: 100+"%"});
    }


    stickerText=$("#sticker .value .text span");
    rows=parent.find(".row");
    if(scrollTop+$(window).innerHeight()/2-$(".page-header nav").outerHeight()/2<rows.eq(0).offset().top){
      stickerText.removeClass("active");
      stickerText.html("00");
    }else if(scrollTop+$(window).innerHeight()/2-$(".page-header nav").outerHeight()/2<rows.eq(1).offset().top){
      stickerText.addClass("active");
      stickerText.html("01");
    }else if(scrollTop+$(window).innerHeight()/2-$(".page-header nav").outerHeight()/2<rows.eq(2).offset().top){
      stickerText.addClass("active");
      stickerText.html("02");
    }else if(scrollTop+$(window).innerHeight()/2-$(".page-header nav").outerHeight()/2<rows.eq(2).offset().top+rows.height()){
      stickerText.addClass("active");
      stickerText.html("03");
    }else{
      stickerText.removeClass("active");
      stickerText.html("03");
    }
  }


  //smoothscroll
  $('.toNextSection').on('click', function(e) {
    currentIndex=$(".NextSection:visible").index($(this).closest(".NextSection"));
    $target=$(".NextSection:visible").eq(currentIndex+1);

    if($target.length>0){
      e.preventDefault();
      $(document).unbind("scroll", scrollBySections);

      $('html, body').stop().animate({
        'scrollTop': $target.offset().top
      }, 500, 'swing', function() {
        // $(document).on("scroll", scrollBySections);
        lastScrollTop=$(document).scrollTop();
      });
    }
  });

  //panel
  $(".panel-group.awesome-panel").on('shown.bs.collapse', function () {
    awesome_panel($(this));
  });
  $(".panel-group.awesome-panel").on('hidden.bs.collapse', function () {
    awesome_panel($(this));
  });
  $(".panel-group").each(function(){
    awesome_panel($(this));
  });
  function awesome_panel(panel_group){
    if(panel_group.length==0)return;
    panel_group.find(".panel-collapse").each(function(){
    panel_heading=$(this).closest(".panel").find(".panel-heading").first();
    if($(this).hasClass("in")){
        panel_heading.addClass("expanded");
      }else{
        panel_heading.removeClass("expanded");
      }
    });
  }
});
//
// jQuery(document).ready(function($) {
//   $(document).on("scroll", onScroll);
//
//   //smoothscroll
//   $('a[href^="#"]').on('click', function(e) {
//     e.preventDefault();
//     $(document).off("scroll");
//
//     $('a').each(function() {
//       $(this).removeClass('active');
//     })
//     $(this).addClass('active');
//
//     var target = this.hash,
//       menu = target;
//     $target = $(target);
//     $('html, body').stop().animate({
//       'scrollTop': $target.offset().top - $('#navbar-section').outerHeight()+1
//     }, 500, 'swing', function() {
//       window.location.hash = target;
//       $(document).on("scroll", onScroll);
//     });
//   });
//
//   function onScroll() {
//     var scrollPos = $(document).scrollTop() + $('#navbar-section').outerHeight();
//     $('.navbar-nav a').each(function() {
//       var currLink = $(this);
//       if(currLink.attr("href").substr(0,1)=='#'&&$(currLink.attr("href")).length){
//         var refElement = $(currLink.attr("href"));
//         if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
//           $('.navbar-nav a').removeClass("active");
//           currLink.addClass("active");
//         } else {
//           currLink.removeClass("active");
//         }
//       }
//     });
//   }
//
//   $('html, body').hide();
//
//   if (location.hash){
//       setTimeout(function(){
//           $('html, body').scrollTop(0).show();
//
//           $('a').each(function() {
//             $(this).removeClass('active');
//           })
//           $(location.hash).addClass('active');
//
//           var target = location.hash,
//             menu = target;
//           $target = $(target);
//           $('html, body').stop().animate({
//             'scrollTop': $target.offset().top - $('#navbar-section').outerHeight()
//           }, 500, 'swing', function() {
//             window.location.hash = target;
//             $(document).on("scroll", onScroll);
//           });
//
//       }, 0);
//   }else{
//       $('html, body').show();
//   }
//
//
// });








// jQuery(document).ready(function($) {
//   $("#leave_a_message .submit").click(function(){
//     var form_data = new FormData();
//     form_data.append('action', 'leave_a_message');
//     form_data.append('name', $("#leave_a_message .name").val());
//     form_data.append('email', $("#leave_a_message .email").val());
//     form_data.append('phone', $("#leave_a_message .phone").val());
//     form_data.append('subject', $("#leave_a_message .subject").val());
//     form_data.append('message', $("#leave_a_message .message").val());
//     $.ajax({
//       type: 'post',
//       url: theme.ajaxurl,
//       dataType: 'text',
//       cache: false,
//       contentType: false,
//       processData: false,
//       data: form_data,
//       beforeSend: function () {
//         $("#leave_a_message .loading").css({display: 'inline-block'});
//         $("#leave_a_message .answer").html('');
//       },
//       success: function (response) {
//         //alert(response);
//
//         $("#leave_a_message .loading").css({display: 'none'});
//         response_object = jQuery.parseJSON(response);
//
//         answer=$("#leave_a_message .answer");
//         answer.html(response_object.text);
//         if(response_object.status=="ok"){
//           answer.css({color:'green'});
//         }else{
//           answer.css({color:'red'});
//         }
//       }
//     });
//   });
// });
